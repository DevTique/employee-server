# Api Funcionarios

A api Permite o Cadastro e Busca de Funcionarios.

## 🔨 Funcionalidades do projecto

- `Funcionalidade 1` - `Cadastro de funcionarios;`

- `Funcionalidade 2` - `Editar de funcionarios registrados;`

- `Funcionalidade 3` - `Listar todos funcionarios registrados;`

- `Funcionalidade 4` - `Listar todos funcionarios registrados de forma paginada;`

- `Funcionalidade 5` - `Pesquisa de funcionarios por filtros;`

- `Funcionalidade 6` - `Remover funcionarios;`

## ✔️ Técnicas e tecnologias utilizadas

- ``Java 17``
- ``Spring Boot``
- ``Postgresql``
- ``Swagger``
- ``Render``
- ``maven``
- ``Paradigma orientado a objectos e funcional``

## 📁 Acesso ao projecto hospedado

Link do projecto hospedado (https://employees-server-0s9p.onrender.com)

## 📁 Acesso ao projecto local
- Clone este repositorio ou baixe o arquivo .zip;

## 🛠️ Abrir e rodar o projecto

- Abrir o projecto na sua IDE favorita;
- Rodar o comando `mvn clean install` no terminal;

##  Documentação  do projecto

- A documentação  esta disponivel em (https://employees-server-0s9p.onrender.com/swagger-ui/)


### Colaborador

- [Luiz Tique Junior](https://www.linkedin.com/in/luiz-tique-j%C3%BAnior-154251229/)