# BUILD
FROM maven:3.9.2-eclipse-temurin-17-alpine as maven

WORKDIR /code
COPY pom.xml .
COPY src ./src

RUN mvn --quiet clean package -DskipTests

# RUN
FROM amazoncorretto:17.0.7-alpine

RUN apk add --no-cache \
    libstdc++ \
    msttcorefonts-installer \
    fontconfig \
    freetype \
    tzdata \
 && update-ms-fonts \
 && adduser -S app_user

WORKDIR /app
RUN chown -R app_user /app

USER app_user

COPY --from=maven /code/target/*.jar /app/app.jar

EXPOSE 8080
CMD ["java", "-jar", "/app/app.jar"]
