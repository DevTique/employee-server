CREATE TABLE tb_employee (
                             id SERIAL NOT NULL PRIMARY KEY,
                             first_name VARCHAR(255) NOT NULL,
                             surname VARCHAR(255) NOT NULL,
                             date_of_birth DATE NOT NULL,
                             hire_date DATE NOT NULL,
                             marital_status VARCHAR(50) NOT NULL,
                             id_number VARCHAR(13) NOT NULL UNIQUE,
                             nuit VARCHAR(9) NOT NULL UNIQUE,
                             created_at TIMESTAMP NOT NULL DEFAULT now(),
                             deleted_at TIMESTAMP,
                             updated_at TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE tb_address (
                            id SERIAL NOT NULL PRIMARY KEY,
                            province VARCHAR(100) NOT NULL,
                            city VARCHAR(100) NOT NULL,
                            street VARCHAR(255) NOT NULL,
                            employee_id BIGINT NOT NULL,
                            FOREIGN KEY (employee_id) REFERENCES tb_employee (id) ON DELETE CASCADE
);

CREATE TABLE tb_contact (
                            id SERIAL NOT NULL PRIMARY KEY,
                            number VARCHAR(255) NOT NULL,
                            employee_id BIGINT NOT NULL,
                            FOREIGN KEY (employee_id) REFERENCES tb_employee (id) ON DELETE CASCADE
);