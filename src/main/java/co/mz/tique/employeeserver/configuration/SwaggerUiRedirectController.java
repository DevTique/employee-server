package co.mz.tique.employeeserver.configuration;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class SwaggerUiRedirectController {
    @GetMapping("/swagger-ui/")
    public String forward() {
        return "forward:/swagger-ui/index.html";
    }
    @GetMapping("/swagger-ui")
    public RedirectView redirect() {
        return new RedirectView("/swagger-ui/");
    }

    @GetMapping("/swagger")
    public RedirectView redirect2() {
        return new RedirectView("/swagger-ui/");
    }
}
