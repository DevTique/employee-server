package co.mz.tique.employeeserver.exception;

import co.mz.tique.employeeserver.exception.exceptions.DuplicatedEmployeeException;
import co.mz.tique.employeeserver.exception.exceptions.EmployeeNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.Instant;


@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(DuplicatedEmployeeException.class)
    private ResponseEntity<ExceptionJson> duplicatedEmployeeExceptionHandler(DuplicatedEmployeeException duplicatedEmployeeException){
       int status = HttpStatus.CONFLICT.value();

        ExceptionJson json = new ExceptionJson();
        json.setTimestamp(Instant.now());
        json.setStatus(status);
        json.setMessage("Usuario Existente, reveja os dados e tente novamente");
        json.setTitle("Usuario Existente");

       return ResponseEntity.status(status).body(json);
    }

    @ExceptionHandler(EmployeeNotFoundException.class)
    private ResponseEntity<ExceptionJson> EmployeeNotFoundExceptionHandler(EmployeeNotFoundException exception){
        int status = HttpStatus.NOT_FOUND.value();

        ExceptionJson json = new ExceptionJson();
        json.setTimestamp(Instant.now());
        json.setStatus(status);
        json.setMessage("Funcionario nao encontrado");
        json.setTitle("Funcionario nao encontrado");

        return ResponseEntity.status(status).body(json);
    }

}