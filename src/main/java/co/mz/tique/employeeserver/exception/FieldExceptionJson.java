package co.mz.tique.employeeserver.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class FieldExceptionJson {
    private String field;
    private String message;
    private int status;
}
