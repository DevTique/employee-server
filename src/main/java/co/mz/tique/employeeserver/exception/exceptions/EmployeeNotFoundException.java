package co.mz.tique.employeeserver.exception.exceptions;

public class EmployeeNotFoundException extends RuntimeException {

    public EmployeeNotFoundException(){}

    public EmployeeNotFoundException(String message) {
        super(message);
    }
}