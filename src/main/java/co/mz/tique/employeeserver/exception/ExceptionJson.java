package co.mz.tique.employeeserver.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Setter
@Getter
@NoArgsConstructor
public class ExceptionJson {
    private Instant timestamp;
    private Integer status;
    private String title;
    private String message;
}
