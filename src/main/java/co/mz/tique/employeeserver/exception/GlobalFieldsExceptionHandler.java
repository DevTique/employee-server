package co.mz.tique.employeeserver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalFieldsExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    private ResponseEntity<FieldExceptionJson> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        FieldError error = ex.getBindingResult().getFieldErrors().get(0);

        int status = HttpStatus.BAD_REQUEST.value();

        FieldExceptionJson json = new FieldExceptionJson();
        json.setField(error.getField());
        json.setStatus(status);
        json.setMessage(error.getDefaultMessage());
        return ResponseEntity.status(status).body(json);
    }
}
