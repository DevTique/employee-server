package co.mz.tique.employeeserver.exception.exceptions;

public class DuplicatedEmployeeException extends RuntimeException{

    public DuplicatedEmployeeException(){}

    public DuplicatedEmployeeException(String message) {
        super(message);
    }
}
