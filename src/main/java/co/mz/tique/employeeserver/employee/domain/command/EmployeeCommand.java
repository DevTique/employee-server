package co.mz.tique.employeeserver.employee.domain.command;

import co.mz.tique.employeeserver.employee.domain.enums.GENRE;
import co.mz.tique.employeeserver.employee.domain.enums.MaritalStatus;
import co.mz.tique.employeeserver.employee.domain.model.Address;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

import java.time.LocalDate;


public record EmployeeCommand(
        @NotNull(message = "O campo 'firstName' é obrigatório")
        @NotBlank(message = "O campo 'firstName' não pode estar vazio")
        String firstName,

        @NotNull(message = "O campo 'surname' é obrigatório")
        @NotBlank(message = "O campo 'surname' não pode estar vazio")
        String surname,

        @NotNull(message = "O campo 'dateOfBirth' é obrigatório")
        LocalDate dateOfBirth,

        @NotNull(message = "O campo 'hireDate' é obrigatório")
        LocalDate hireDate,

        @NotNull(message = "O campo 'maritalStatus' é obrigatório")
        MaritalStatus maritalStatus,

        @NotNull(message = "O campo 'idNumber' é obrigatório")
        @NotBlank(message = "O campo 'idNumber' não pode estar vazio")
        @Pattern(regexp = "^\\d{12}[A-Z]$", message = "O campo 'idNumber' deve ter 13 caracteres e o último deve ser uma letra maiúscula.")
        String idNumber,

        @NotNull(message = "O campo 'nuit' é obrigatório")
        @NotBlank(message = "O campo 'nuit' não pode estar vazio")
        String nuit,

        @NotNull(message = "O campo 'phoneNumber' é obrigatório")
        @NotBlank(message = "O campo 'phoneNumber' não pode estar vazio")
        @Pattern(regexp = "^(82|83|84|85|86|87)\\d{7}$", message = "O número de telefone deve começar com 82, 83, 84, 85, 86, ou 87 e ter 9 dígitos.")
        String phoneNumber,

        @NotNull(message = "O campo 'genre' é obrigatório")
        @NotBlank(message = "O campo 'genre' não pode estar vazio")
        GENRE genre,

        Address address
) {}
