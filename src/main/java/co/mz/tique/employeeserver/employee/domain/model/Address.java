package co.mz.tique.employeeserver.employee.domain.model;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Embeddable
public class Address {
    private String province;
    private String city;
    private String street;
}
