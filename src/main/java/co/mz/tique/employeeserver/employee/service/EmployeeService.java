package co.mz.tique.employeeserver.employee.service;


import co.mz.tique.employeeserver.employee.domain.command.EmployeeCommand;
import co.mz.tique.employeeserver.employee.domain.model.Employee;
import co.mz.tique.employeeserver.employee.domain.model.EmployeeQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EmployeeService {
    Employee createEmployee(EmployeeCommand employeeCommand);

    Page<Employee> getAllEmployees(EmployeeQuery query, Pageable pageable);

    List<Employee> getAllEmployees(EmployeeQuery query);

    Employee getEmployeeById(Long id);

    Employee updateEmployee(Long id, EmployeeCommand employeeCommand);

    void deleteEmployee(Long id);
}
