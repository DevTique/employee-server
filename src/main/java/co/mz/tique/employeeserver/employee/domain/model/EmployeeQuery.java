package co.mz.tique.employeeserver.employee.domain.model;

import co.mz.tique.employeeserver.employee.domain.enums.MaritalStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeQuery {
    @DateTimeFormat(pattern ="dd-MM-yyyy")
    LocalDate startDate;
    @DateTimeFormat(pattern ="dd-MM-yyyy")
    LocalDate endDate;
    String firstName;
    String surname;
    LocalDate dateOfBirth;
    LocalDate hireDate;
    MaritalStatus maritalStatus;
    Long addressId;
    String idNumber;
    String nuit;
}
