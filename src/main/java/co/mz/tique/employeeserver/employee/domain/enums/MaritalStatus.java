package co.mz.tique.employeeserver.employee.domain.enums;

public enum MaritalStatus {
    MARRIED, SINGLE, DIVORCED
}