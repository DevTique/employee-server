package co.mz.tique.employeeserver.employee.service;

import co.mz.tique.employeeserver.employee.domain.command.EmployeeCommand;
import co.mz.tique.employeeserver.employee.domain.mapper.EmployeeMapper;
import co.mz.tique.employeeserver.employee.domain.model.Employee;
import co.mz.tique.employeeserver.employee.domain.model.EmployeeQuery;
import co.mz.tique.employeeserver.employee.persistence.EmployeeRepository;
import co.mz.tique.employeeserver.exception.exceptions.DuplicatedEmployeeException;
import co.mz.tique.employeeserver.exception.exceptions.EmployeeNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final EmployeeSpecification employeeSpecification;

    @Transactional
    public Employee createEmployee(EmployeeCommand employeeCommand){
         try {
             Employee employee = EmployeeMapper.INSTANCE.mapToModel(employeeCommand);

             return  employeeRepository.save(employee);
         }catch (DataIntegrityViolationException ex){
             throw new DuplicatedEmployeeException();
         }
    }

    @Transactional(readOnly = true)
    public Page<Employee> getAllEmployees(EmployeeQuery query, Pageable pageable) {
        return employeeRepository.findAll(employeeSpecification.executeQuery(query), pageable);
    }

    @Transactional(readOnly = true)
    public List<Employee> getAllEmployees(EmployeeQuery query) {
        return employeeRepository.findAll(employeeSpecification.executeQuery(query));
    }

    @Transactional(readOnly = true)
    public Employee getEmployeeById(Long id) {
        return checkEmployee(id);
    }

    @Transactional
    public Employee updateEmployee(Long id, EmployeeCommand employeeCommand) {
        Employee employee = checkEmployee(id);

        employee = EmployeeMapper.INSTANCE.updateModel(employeeCommand, employee);

        return employeeRepository.save(employee);
    }

    public void deleteEmployee(Long id) {
        Employee employee = checkEmployee(id);
        employeeRepository.delete(employee);
    }

    private Employee checkEmployee(Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }
}
