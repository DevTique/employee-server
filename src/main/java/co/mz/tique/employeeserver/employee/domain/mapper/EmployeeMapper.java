package co.mz.tique.employeeserver.employee.domain.mapper;

import co.mz.tique.employeeserver.employee.domain.model.Employee;
import co.mz.tique.employeeserver.employee.domain.command.EmployeeCommand;
import co.mz.tique.employeeserver.employee.presentation.controller.json.EmployeeJson;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);

    Employee mapToModel(EmployeeCommand employeeCommand);

    EmployeeCommand modelToCommand(Employee employee);

    EmployeeJson modelToJson(Employee employee);

    default Page<EmployeeJson> modelToJson(Page<Employee> employees) {
        return employees.map(this::modelToJson);
    }

    default List<EmployeeJson> modelToJson(List<Employee> employees) {
        return employees.stream().map(this::modelToJson).toList();
    }

    @Mapping(target = "id", ignore = true)
    Employee updateModel(EmployeeCommand employeeCommand, @MappingTarget Employee employee);
}