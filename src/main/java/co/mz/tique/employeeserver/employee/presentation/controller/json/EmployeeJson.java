package co.mz.tique.employeeserver.employee.presentation.controller.json;

import co.mz.tique.employeeserver.employee.domain.enums.GENRE;
import co.mz.tique.employeeserver.employee.domain.enums.MaritalStatus;
import co.mz.tique.employeeserver.employee.domain.model.Address;

import java.time.LocalDate;

public record EmployeeJson(
         Long id,
         String firstName,
         String surname,
         LocalDate dateOfBirth,
         LocalDate hireDate,
         MaritalStatus maritalStatus,
         GENRE genre,
         String idNumber,
         String phoneNumber,
         String nuit,
         Address address){}
