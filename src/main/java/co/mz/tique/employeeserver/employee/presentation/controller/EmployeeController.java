package co.mz.tique.employeeserver.employee.presentation.controller;

import co.mz.tique.employeeserver.employee.domain.command.EmployeeCommand;
import co.mz.tique.employeeserver.employee.domain.mapper.EmployeeMapper;
import co.mz.tique.employeeserver.employee.domain.model.EmployeeQuery;
import co.mz.tique.employeeserver.employee.presentation.controller.json.EmployeeJson;
import co.mz.tique.employeeserver.employee.service.EmployeeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping
    public ResponseEntity<EmployeeJson> createEmployee(@Valid  @RequestBody EmployeeCommand employeeCommand) {
        return ResponseEntity.status(HttpStatus.CREATED).body(EmployeeMapper.INSTANCE.modelToJson(employeeService.createEmployee(employeeCommand)));
    }

    @GetMapping
    public ResponseEntity<List<EmployeeJson>> getAllEmployees(EmployeeQuery query) {
        return ResponseEntity.ok(EmployeeMapper.INSTANCE.modelToJson(employeeService.getAllEmployees(query)));
    }

    @GetMapping("/paged")
    public ResponseEntity<Page<EmployeeJson>> getAllEmployeesPaged(EmployeeQuery query, Pageable pageable) {
        return ResponseEntity.ok(EmployeeMapper.INSTANCE.modelToJson(employeeService.getAllEmployees(query, pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeJson> getEmployeeById(@PathVariable Long id) {
        return ResponseEntity.ok(EmployeeMapper.INSTANCE.modelToJson(employeeService.getEmployeeById(id)));
    }

    @PutMapping("/{id}")
    public ResponseEntity<EmployeeJson> updateEmployee(@PathVariable Long id, @Valid @RequestBody EmployeeCommand employeeCommand) {
        return ResponseEntity.ok(EmployeeMapper.INSTANCE.modelToJson(employeeService.updateEmployee(id, employeeCommand)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
        return ResponseEntity.noContent().build();
    }

}