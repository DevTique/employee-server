package co.mz.tique.employeeserver.employee.service;

import co.mz.tique.employeeserver.employee.domain.enums.MaritalStatus;
import co.mz.tique.employeeserver.employee.domain.model.Employee;
import co.mz.tique.employeeserver.employee.domain.model.EmployeeQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class EmployeeSpecification {

    public Specification<Employee> executeQuery(EmployeeQuery query) {
        LocalDateTime startDate=query.getStartDate()==null?LocalDateTime.of(2000,1,1,0,0):query.getStartDate().atStartOfDay();
        LocalDateTime endDate=query.getEndDate()==null?LocalDate.now().atTime(23,59,59):query.getEndDate().atTime(23,59,59);

        return all()
                .and(findByHireDateInterval(startDate,endDate))
                .and(findByFirstName(query.getFirstName()))
                .and(findBySurname(query.getSurname()))
                .and(findByDateOfBirth(query.getDateOfBirth()))
                .and(findByHireDate(query.getHireDate()))
                .and(findByMaritalStatus(query.getMaritalStatus()))
                .and(findByIdNumber(query.getIdNumber()))
                .and(findByNuit(query.getNuit()));
    }

    public Specification<Employee> findByFirstName(String firstName) {
        return (root, cq, cb) -> firstName == null ? null : cb.like(cb.lower(root.get("firstName")), "%" + firstName.toLowerCase() + "%");
    }

    public Specification<Employee> findBySurname(String surname) {
        return (root, cq, cb) -> surname == null ? null : cb. like(cb.lower(root.get("surname")), "%" + surname.toLowerCase() + "%");
    }

    public Specification<Employee> findByDateOfBirth(LocalDate dateOfBirth) {
        return (root, cq, cb) -> dateOfBirth == null ? null : cb.equal(root.get("dateOfBirth"), dateOfBirth);
    }

    public Specification<Employee> findByHireDate(LocalDate hireDate) {
        return (root, cq, cb) -> hireDate == null ? null : cb.equal(root.get("hireDate"), hireDate);
    }

    public Specification<Employee> findByHireDateInterval(LocalDateTime startDate,LocalDateTime endDate) {
        return (root, query, criteriaBuilder)->criteriaBuilder.between(root.get("hireDate"),startDate,endDate);
    }

    public Specification<Employee> findByMaritalStatus(MaritalStatus maritalStatus) {
        return (root, cq, cb) -> maritalStatus == null ? null : cb.equal(root.get("maritalStatus"), maritalStatus);
    }

    public Specification<Employee> findByIdNumber(String idNumber) {
        return (root, cq, cb) -> idNumber == null ? null : cb.equal(root.get("idNumber"), idNumber);
    }

    public Specification<Employee> findByNuit(String nuit) {
        return (root, cq, cb) -> nuit == null ? null : cb.equal(root.get("nuit"), nuit);
    }

    private Specification<Employee> all() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and();
    }
}
