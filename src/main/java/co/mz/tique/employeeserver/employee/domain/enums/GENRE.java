package co.mz.tique.employeeserver.employee.domain.enums;

public enum GENRE {
    MALE, FEMALE
}
