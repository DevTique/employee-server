package co.mz.tique.employeeserver.service;

import co.mz.tique.employeeserver.BaseEmployeeTest;
import co.mz.tique.employeeserver.employee.domain.command.EmployeeCommand;
import co.mz.tique.employeeserver.employee.domain.model.Employee;
import co.mz.tique.employeeserver.employee.domain.model.EmployeeQuery;
import co.mz.tique.employeeserver.employee.persistence.EmployeeRepository;
import co.mz.tique.employeeserver.employee.service.EmployeeServiceImpl;
import co.mz.tique.employeeserver.employee.service.EmployeeSpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest extends BaseEmployeeTest {

    @Mock
    private EmployeeRepository repository;

    @Mock
    private EmployeeServiceImpl underTest;

    @Mock
    private EmployeeSpecification employeeSpecification;

    @BeforeEach
    void setUp() {
        underTest = new EmployeeServiceImpl(repository, employeeSpecification);
    }

    @Test
    void create() {
        // given
        EmployeeCommand command = getEmployeeCommand();
        Employee employee = getEmployee(command);

        // when
        when(repository.save(Mockito.any())).thenReturn(employee);
        Employee createdCommand = underTest.createEmployee(command);

        // then
        verify(repository).save(Mockito.any());
        verifyNoMoreInteractions(repository);

        Assertions.assertNotNull(createdCommand);
        Assertions.assertNotNull(employee);
    }

    @Test
    void update() {
        // given
        Long id = faker().number().randomNumber();
        EmployeeCommand updateCommand = getEmployeeCommand();
        Employee employee = getEmployee(updateCommand);

        // when
        when(repository.findById(id)).thenReturn(Optional.of(employee));
        when(repository.save(employee)).thenReturn(employee);
        Employee updatedCommand = underTest.updateEmployee(id, updateCommand);

        // then
        verify(repository).findById(id);
        verify(repository).save(employee);
        verifyNoMoreInteractions(repository);
        Assertions.assertNotNull(updatedCommand);
    }

    @Test
    void findById() {
        // given
        Employee employee = getAnyEmployee();
        Long id = employee.getId();

        // when
        when(repository.findById(id)).thenReturn(Optional.of(employee));

        // then
        Employee foundCommand = underTest.getEmployeeById(id);
        verify(repository).findById(id);
        verifyNoMoreInteractions(repository);

        Assertions.assertNotNull(foundCommand);
    }

    @Test
    void findAllUnpaged() {
        // given
        Page<Employee> page = getAnyEmployeePage();
        EmployeeQuery query = getAnyEmployeeQuery();

        // when
        when(repository.findAll(employeeSpecification.executeQuery(query), Pageable.unpaged())).thenReturn(page);
        verifyNoMoreInteractions(repository);

        // then
        Page<Employee> foundCommands = underTest.getAllEmployees(query, Pageable.unpaged());
        verify(repository).findAll(employeeSpecification.executeQuery(query), Pageable.unpaged());
        verifyNoMoreInteractions(repository);
        Assertions.assertNotNull(foundCommands);
        Assertions.assertNotNull(foundCommands.getContent());
        Assertions.assertEquals(foundCommands.getTotalElements(), page.getTotalElements());
    }

    @Test
    void findAllPaged() {
        // given
        Pageable pageable = PageRequest.of(0, 10);
        Page<Employee> page = getAnyEmployeePage();
        EmployeeQuery query = getAnyEmployeeQuery();

        // when
        when(repository.findAll(employeeSpecification.executeQuery(query), pageable)).thenReturn(page);
        verifyNoMoreInteractions(repository);

        // then
        Page<Employee> foundCommands = underTest.getAllEmployees(query, pageable);
        verify(repository).findAll(employeeSpecification.executeQuery(query), pageable);
        verifyNoMoreInteractions(repository);
        Assertions.assertNotNull(foundCommands);
        Assertions.assertNotNull(foundCommands.getContent());
        Assertions.assertEquals(foundCommands.getTotalElements(), page.getTotalElements());
    }


}