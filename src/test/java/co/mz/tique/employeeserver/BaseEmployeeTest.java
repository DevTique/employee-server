package co.mz.tique.employeeserver;

import co.mz.tique.employeeserver.employee.domain.command.EmployeeCommand;
import co.mz.tique.employeeserver.employee.domain.mapper.EmployeeMapper;
import co.mz.tique.employeeserver.employee.domain.model.Employee;
import co.mz.tique.employeeserver.employee.domain.model.EmployeeQuery;
import com.github.javafaker.Faker;
import org.instancio.Instancio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseEmployeeTest {
    private final Faker faker = new Faker();

    protected final Faker faker() {
        return faker;
    }

    protected Employee getAnyEmployee() {
        Employee employee = EmployeeMapper.INSTANCE.mapToModel(getEmployeeCommand());
         employee.setId(faker().number().numberBetween(1L, 100L));
        return employee;
    }

    protected Employee getEmployee(EmployeeCommand command) {
        return EmployeeMapper.INSTANCE.mapToModel(command);
    }

    protected Page<Employee> getAnyEmployeePage() {
        return new PageImpl<Employee>(getAnyEmployeeList());
    }

    protected List<Employee> getAnyEmployeeList() {
        List<Employee> employees = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            employees.add(getAnyEmployee());
        }
        return employees;
    }

    protected EmployeeCommand getEmployeeCommand() {
        return Instancio.create(EmployeeCommand.class);
    }

    protected EmployeeQuery getAnyEmployeeQuery() {
        return Instancio.create(EmployeeQuery.class);
    }
}
